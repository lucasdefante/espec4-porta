package br.com.itau.porta.dtos;

import br.com.itau.porta.models.Porta;
import org.springframework.stereotype.Component;

@Component
public class PortaMapper {

    public Porta toPorta(CriarPortaDTO criarPortaDTO) {
        Porta porta = new Porta();
        porta.setAndar(criarPortaDTO.getAndar());
        porta.setSala(criarPortaDTO.getSala());
        return porta;
    }
}
