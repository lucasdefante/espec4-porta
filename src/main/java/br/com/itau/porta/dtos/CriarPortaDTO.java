package br.com.itau.porta.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CriarPortaDTO {

    @NotNull(message = "Andar não pode ser nulo")
    @NotBlank(message = "Andar não pode ser branco")
    private String andar;

    @NotNull(message = "Sala não pode ser nulo")
    @NotBlank(message = "Sala não pode ser branco")
    private String sala;

    public CriarPortaDTO() {
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
