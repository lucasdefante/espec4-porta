package br.com.itau.porta.controllers;

import br.com.itau.porta.dtos.CriarPortaDTO;
import br.com.itau.porta.dtos.PortaMapper;
import br.com.itau.porta.models.Porta;
import br.com.itau.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @Autowired
    private PortaMapper portaMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta criarPorta(@RequestBody @Valid CriarPortaDTO criarPortaDTO){
        try {
            Porta porta = portaMapper.toPorta(criarPortaDTO);
            return portaService.criarPorta(porta);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Porta consultarPortaPorId(@PathVariable(name = "id") int id) {
        return portaService.consultarPortaPorId(id);
    }

}
