package br.com.itau.porta.services;

import br.com.itau.porta.exceptions.PortaNotFoundException;
import br.com.itau.porta.models.Porta;
import br.com.itau.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta criarPorta(Porta porta) {
        try {
            return portaRepository.save(porta);
        } catch (DataIntegrityViolationException e) {
            throw new RuntimeException("Sala já cadastrada no sistema.");
        }
    }

    public Porta consultarPortaPorId(int id) {
        Optional<Porta> optionalPorta = portaRepository.findById(id);
        if (!optionalPorta.isPresent()) {
            throw new PortaNotFoundException();
        }
        return optionalPorta.get();
    }
}
